﻿/*
 * Simple Asset Filter Post Processor
 * Removes useless transforms from a model asset at the post process stage.
 * Useful for removing helper objects, custom bone shapes, empties, etc, from a mesh to avoid cluttering of your objects hierarchy.
 *
 * To be placed in an /editor/ folder
 * */
 
using UnityEngine;
using System.Collections;
using UnityEditor;
 
public class CharacterAssetProcessor : AssetPostprocessor {
 
        // We search for this in the assets file name so that we know we should process it.
        const string AssetFileIdentifier = "Character_";
 
        // Within the asset all files containing this string will be filtered out
        const string AssetFilterIdentifier = "SHP_";
 
 
 
        private void OnPostprocessModel (GameObject gO)
        {
                if (gO.name.Contains( AssetFileIdentifier ))
                {
                        Debug.Log ("Post processing " + gO.name);
                        foreach (Transform t in gO.GetComponentsInChildren <Transform>())
                        {
                                if (t.name.Contains (AssetFilterIdentifier))
                                {
                                        Debug.Log ("Deleting " + t.name);
                                        Object.DestroyImmediate(t.gameObject);
                                }
                        }
                        Debug.Log ("Done.");
                }
        }
}