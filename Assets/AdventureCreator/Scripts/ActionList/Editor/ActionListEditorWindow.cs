﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using AC;

public class ActionListEditorWindow : EditorWindow
{
	
	private bool isMarquee = false;
	private Rect marqueeRect = new Rect (0f, 0f, 0f, 0f);
	private bool canMarquee = true;
	private bool marqueeShift = false;
	private bool isAutoArranging = false;
	
	private float zoom = 1f;
	private float zoomMin = 0.5f;
	private float zoomMax = 1f;
	
	private Action actionChanging = null;
	private bool resultType;
	private int multipleResultType;
	private int offsetChanging = 0;
	private int numActions = 0;

	private ActionList _target;
	private ActionListAsset _targetAsset;
	private Vector2 scrollPosition = Vector2.zero;
	private Vector2 maxScroll;
	private Vector2 menuPosition;
	
	private GUISkin emptyNodeSkin = (GUISkin) AssetDatabase.LoadAssetAtPath ("Assets/AdventureCreator/Graphics/Skins/ACEmptyNodeSkin.guiskin", typeof (GUISkin));
	private GUISkin nodeSkin = (GUISkin) AssetDatabase.LoadAssetAtPath ("Assets/AdventureCreator/Graphics/Skins/ACNodeSkin.guiskin", typeof (GUISkin));
	
	private ActionsManager actionsManager;
	
	
	[MenuItem ("Adventure Creator/Editors/ActionList Editor")]
	static void Init ()
	{
		ActionListEditorWindow window = (ActionListEditorWindow) EditorWindow.GetWindow (typeof (ActionListEditorWindow));
		window.Repaint ();
		window.title = "ActionList Editor";
	}
	
	
	private void OnEnable ()
	{
		if (AdvGame.GetReferences () && AdvGame.GetReferences ().actionsManager)
		{
			actionsManager = AdvGame.GetReferences ().actionsManager;
		}
		
		if (_targetAsset != null)
		{
			UnmarkAll (true);
		}
		else
		{
			UnmarkAll (false);
		}
	}
	
	
	private void PanAndZoomWindow ()
	{
		if (actionChanging)
		{
			return;
		}
		
		if (Event.current.type == EventType.ScrollWheel)
		{
			Vector2 screenCoordsMousePos = Event.current.mousePosition;
			Vector2 delta = Event.current.delta;
			float zoomDelta = -delta.y / 80.0f;
			float oldZoom = zoom;
			zoom += zoomDelta;
			zoom = Mathf.Clamp(zoom, zoomMin, zoomMax);
			scrollPosition += (screenCoordsMousePos - scrollPosition) - (oldZoom / zoom) * (screenCoordsMousePos - scrollPosition);
			
			Event.current.Use();
		}
		
		if (Event.current.type == EventType.MouseDrag && Event.current.button == 2)
		{
			Vector2 delta = Event.current.delta;
			delta /= zoom;
			scrollPosition -= delta;
			
			Event.current.Use();
		}
	}
	
	
	private void DrawMarquee (bool isAsset)
	{
		if (actionChanging)
		{
			return;
		}
		
		if (!canMarquee)
		{
			isMarquee = false;
			return;
		}

		Event e = Event.current;
		
		if (e.type == EventType.MouseDown && e.button == 0 && !isMarquee)
		{
			if (e.mousePosition.y > 24)
			{
				isMarquee = true;
				marqueeShift = false;
				marqueeRect = new Rect (e.mousePosition.x, e.mousePosition.y, 0f, 0f);
			}
		}
		else if (e.rawType == EventType.MouseUp)
		{
			if (isMarquee)
			{
				MarqueeSelect (isAsset, marqueeShift);
			}
			isMarquee = false;
		}
		if (isMarquee && e.shift)
		{
			marqueeShift = true;
		}

		if (isMarquee)
		{
			marqueeRect.width = e.mousePosition.x - marqueeRect.x;
			marqueeRect.height = e.mousePosition.y - marqueeRect.y;
			GUI.Label (marqueeRect, "", nodeSkin.customStyles[9]);
		}
	}
	
	
	private void MarqueeSelect (bool isAsset, bool isCumulative)
	{
		List<Action> actionList = new List<Action>();
		if (isAsset)
		{
			actionList = _targetAsset.actions;
		}
		else
		{
			actionList = _target.actions;
		}
		
		if (marqueeRect.width < 0f)
		{
			marqueeRect.x += marqueeRect.width;
			marqueeRect.width *= -1f;
		}
		if (marqueeRect.height < 0f)
		{
			marqueeRect.y += marqueeRect.height;
			marqueeRect.height *= -1f;
		}
		
		// Correct for zooming
		marqueeRect.x /= zoom;
		marqueeRect.y /= zoom;
		marqueeRect.width /= zoom;
		marqueeRect.height /= zoom;
		
		// Correct for panning
		marqueeRect.x += scrollPosition.x;
		marqueeRect.y += scrollPosition.y;

		marqueeRect.y -= 18f;

		if (!isCumulative)
		{
			UnmarkAll (isAsset);
		}

		foreach (Action action in actionList)
		{
			if (IsRectInRect (action.nodeRect, marqueeRect) || IsRectInRect (marqueeRect, action.nodeRect))
			{
				action.isMarked = true;
			}
		}
	}


	private bool IsRectInRect (Rect rect1, Rect rect2)
	{
		if (rect1.Contains (rect2.BottomRight ()) || rect1.Contains (rect2.BottomLeft ()) || rect1.Contains (rect2.TopLeft ()) || rect1.Contains (rect2.TopRight ()))
		{
			return true;
		}
		return false;
	}

	
	private void OnGUI ()
	{
		if (isAutoArranging)
		{
			return;
		}

		if (Selection.activeObject && Selection.activeObject is ActionListAsset)
		{
			_targetAsset = (ActionListAsset) Selection.activeObject;
			_target = null;
		}
		else if (Selection.activeGameObject && Selection.activeGameObject.GetComponent <ActionList>())
		{
			_targetAsset = null;
			_target = Selection.activeGameObject.GetComponent<ActionList>();
		}

		if (_targetAsset != null)
		{
			ActionListAssetEditor.ResetList (_targetAsset);
			
			PanAndZoomWindow ();
			NodesGUI (true);
			DrawMarquee (true);
			
			TopToolbarGUI (true);
			
			if (GUI.changed)
			{
				EditorUtility.SetDirty (_targetAsset);
			}
		}
		else if (_target != null)
		{
			ActionListEditor.ResetList (_target);
			
			if (_target.source != ActionListSource.AssetFile)
			{
				PanAndZoomWindow ();
				NodesGUI (false);
				DrawMarquee (false);
			}

			TopToolbarGUI (false);
			
			if (GUI.changed)
			{
				EditorUtility.SetDirty (_target);
			}
		}
		else
		{
			TopToolbarGUI (false);
		}
	}


	private void TopToolbarGUI (bool isAsset)
	{
		bool noList = false;
		bool showLabel = false;
		float buttonWidth = 20f;
		if (position.width > 600)
		{
			buttonWidth = 60f;
			showLabel = true;
		}

		if ((isAsset && _targetAsset == null) || (!isAsset && _target == null))
		{
			noList = true;
		}

		GUILayout.BeginArea (new Rect (0,0,position.width,24), nodeSkin.box);

		float midX = position.width * 0.4f;

		if (showLabel)
		{
			string label;
			if (noList)
			{
				label = "No ActionList selected";
			}
			else if (isAsset)
			{
				label = "Editing asset: " + _targetAsset.name;
			}
			else
			{
				if (_target.source == ActionListSource.AssetFile)
				{
					label = "Cannot view Actions, since this object references an Asset file.";
				}
				else
				{
					label = "Editing scene list: " + _target.gameObject.name;
				}
			}
			GUI.Label (new Rect (10,2,150,20), label, nodeSkin.customStyles[8]);
		}

		if (noList)
		{
			GUI.enabled = false;
		}

		if (ToolbarButton (midX-buttonWidth-buttonWidth, buttonWidth, showLabel, "Insert", 7))
		{
			menuPosition = new Vector2 (70f, 30f);
			PerformEmptyCallBack ("Add new Action");
		}

		if (!noList && NumActionsMarked (isAsset) > 0)
		{
			GUI.enabled = true;
		}
		else
		{
			GUI.enabled = false;
		}
		
		if (ToolbarButton (midX-buttonWidth, buttonWidth, showLabel, "Delete", 5))
		{
			PerformEmptyCallBack ("Delete selected");
		}

		if (!noList)
		{
			GUI.enabled = true;
		}

		if (ToolbarButton (position.width-(buttonWidth*3f), buttonWidth*1.5f, showLabel, "Auto-arrange", 6))
		{
			AutoArrange (isAsset);
		}

		if (noList)
		{
			GUI.enabled = false;
		}
		else
		{
			GUI.enabled = Application.isPlaying;
		}

		if (ToolbarButton (position.width-buttonWidth, buttonWidth, showLabel, "Run", 4))
		{
			if (isAsset)
			{
				AdvGame.RunActionListAsset (_targetAsset);
			}
			else
			{
				_target.Interact ();
			}
		}

		if (!noList && NumActionsMarked (isAsset) > 0)
		{
			GUI.enabled = true;
		}
		else
		{
			GUI.enabled = false;
		}

		if (ToolbarButton (midX, buttonWidth, showLabel, "Copy", 1))
		{
			PerformEmptyCallBack ("Copy selected");
		}

		if (ToolbarButton (midX+buttonWidth, buttonWidth, showLabel, "Cut", 3))
		{
			PerformEmptyCallBack ("Cut selected");
		}

		if (!noList && AdvGame.copiedActions != null && AdvGame.copiedActions.Count > 0)
		{
			GUI.enabled = true;
		}
		else
		{
			GUI.enabled = false;
		}

		if (ToolbarButton (midX+buttonWidth+buttonWidth, buttonWidth, showLabel, "Paste", 2))
		{
			EmptyCallback ("Paste copied Action(s)");
		}

		GUI.enabled = true;

		GUILayout.EndArea ();
	}


	private bool ToolbarButton (float startX, float width, bool showLabel, string label, int styleIndex)
	{
		if (showLabel)
		{
			return GUI.Button (new Rect (startX,2,width,20), label, nodeSkin.customStyles[styleIndex]);
		}
		return GUI.Button (new Rect (startX,2,20,20), "", nodeSkin.customStyles[styleIndex]);
	}
	
	
	private void OnInspectorUpdate ()
	{
		Repaint();
	}


	private void NodeWindow (int i)
	{
		GUI.skin = null;
		
		if (actionsManager == null)
		{
			OnEnable ();
		}
		if (actionsManager == null)
		{
			return;
		}
		
		bool isAsset;
		Action _action;
		List<ActionParameter> parameters = null;
		
		if (_targetAsset != null)
		{
			_action = _targetAsset.actions[i];
			isAsset = _action.isAssetFile = true;
			if (_targetAsset.useParameters)
			{
				parameters = _targetAsset.parameters;
			}
		}
		else
		{
			_action = _target.actions[i];
			isAsset = _action.isAssetFile = false;
			if (_target.useParameters)
			{
				parameters = _target.parameters;
			}
		}
		
		if (!actionsManager.DoesActionExist (_action.GetType ().ToString ()))
		{
			EditorGUILayout.HelpBox ("This Action type has been disabled in the Actions Manager", MessageType.Warning);
		}
		else
		{
			int typeNumber = GetTypeNumber (i, isAsset);
			int categoryNumber = GetCategoryNumber (typeNumber);
			int subCategoryNumber = GetSubCategoryNumber (i, categoryNumber, isAsset);
			
			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.LabelField ("Action type:", GUILayout.Width (100));
			categoryNumber = EditorGUILayout.Popup (categoryNumber, actionsManager.GetActionCategories ());
			subCategoryNumber = EditorGUILayout.Popup (subCategoryNumber, actionsManager.GetActionSubCategories (categoryNumber));
			EditorGUILayout.EndHorizontal ();
			
			typeNumber = actionsManager.GetTypeNumber (categoryNumber, subCategoryNumber);
			
			EditorGUILayout.Space ();
			
			// Rebuild constructor if Subclass and type string do not match
			if (_action.GetType ().ToString () != actionsManager.GetActionName (typeNumber) && _action.GetType ().ToString () != ("AC." + actionsManager.GetActionName (typeNumber)))
			{
				Vector2 currentPosition = new Vector2 (_action.nodeRect.x, _action.nodeRect.y);
				
				// Store "After running data" to transfer over
				ActionEnd _end = new ActionEnd ();
				_end.resultAction = _action.endAction;
				_end.skipAction = _action.skipAction;
				_end.linkedAsset = _action.linkedAsset;
				_end.linkedCutscene = _action.linkedCutscene;
				
				if (isAsset)
				{
					Undo.RecordObject (_targetAsset, "Change Action type");
					
					Action newAction = ActionListAssetEditor.RebuildAction (_action, typeNumber, _targetAsset, _end.resultAction, _end.skipAction, _end.linkedAsset, _end.linkedCutscene);
					newAction.nodeRect.x = currentPosition.x;
					newAction.nodeRect.y = currentPosition.y;
					
					_targetAsset.actions.Remove (_action);
					_targetAsset.actions.Insert (i, newAction);
				}
				else
				{
					Undo.RecordObject (_target, "Change Action type");
					
					Action newAction = ActionListEditor.RebuildAction (_action, typeNumber, _end.resultAction, _end.skipAction, _end.linkedAsset, _end.linkedCutscene);
					newAction.nodeRect.x = currentPosition.x;
					newAction.nodeRect.y = currentPosition.y;
					
					_target.actions.Remove (_action);
					_target.actions.Insert (i, newAction);
				}
			}
			
			_action.ShowGUI (parameters);
		}
		
		if (_action.endAction == ResultAction.Skip || _action.numSockets == 2 || _action is ActionCheckMultiple)
		{
			if (isAsset)
			{
				_action.SkipActionGUI (_targetAsset.actions, true);
			}
			else
			{
				_action.SkipActionGUI (_target.actions, true);
			}
		}
		
		_action.isDisplayed = EditorGUI.Foldout (new Rect (10,1,20,16), _action.isDisplayed, "");
		
		if (GUI.Button (new Rect(273,3,16,16), " ", nodeSkin.customStyles[0]))
		{
			CreateNodeMenu (isAsset, i, _action);
		}
		
		if (i == 0)
		{
			_action.nodeRect.x = 14;
			_action.nodeRect.y = 14;
		}
		else
		{
			if (Event.current.button == 0)
			{
				GUI.DragWindow ();
			}
		}
	}
	
	
	private void EmptyNodeWindow (int i)
	{
		Action _action;
		bool isAsset = false;
		
		if (_targetAsset != null)
		{
			_action = _targetAsset.actions[i];
			isAsset = true;
		}
		else
		{
			_action = _target.actions[i];
		}
		
		_action.isDisplayed = EditorGUI.Foldout (new Rect (10,1,20,16), _action.isDisplayed, "");

		if (GUI.Button (new Rect(273,3,16,16), " ", nodeSkin.customStyles[0]))
		{
			CreateNodeMenu (isAsset, i, _action);
		}

		if (i == 0)
		{
			_action.nodeRect.x = 14;
			_action.nodeRect.y = 14;
		}
		else
		{
			if (Event.current.button == 0)
			{
				GUI.DragWindow ();
			}
		}
	}
	
	
	private void LimitWindow (Action action)
	{
		if (action.nodeRect.x < 1)
		{
			action.nodeRect.x = 1;
		}
		
		if (action.nodeRect.y < 14)
		{
			action.nodeRect.y = 14;
		}
	}
	
	
	private void NodesGUI (bool isAsset)
	{
		if (AdvGame.GetReferences () && AdvGame.GetReferences ().actionsManager)
		{
			actionsManager = AdvGame.GetReferences ().actionsManager;
		}
	
		bool loseConnection = false;
		Event e = Event.current;

		if (e.isMouse && actionChanging != null)
		{
			if (e.type == EventType.MouseUp)
			{
				loseConnection = true;
			}
			else if (e.mousePosition.x < 0f || e.mousePosition.x > position.width || e.mousePosition.y < 0f || e.mousePosition.y > position.height)
			{
				loseConnection = true;
				actionChanging = null;
			}
		}
		
		if (isAsset)
		{
			numActions = _targetAsset.actions.Count;
			if (numActions < 1)
			{
				numActions = 1;
				AC.Action newAction = ActionListEditor.GetDefaultAction ();
				newAction.hideFlags = HideFlags.HideInHierarchy;
				_targetAsset.actions.Add (newAction);
				AssetDatabase.AddObjectToAsset (newAction, _targetAsset);
				AssetDatabase.SaveAssets ();
			}
			numActions = _targetAsset.actions.Count;
		}
		else
		{
			numActions = _target.actions.Count;
			if (numActions < 1)
			{
				numActions = 1;
				AC.Action newAction = ActionListEditor.GetDefaultAction ();
				_target.actions.Add (newAction);
			}
			numActions = _target.actions.Count;
		}

		EditorZoomArea.Begin (zoom, new Rect (0, 0, position.width / zoom, position.height / zoom));
		scrollPosition = GUI.BeginScrollView (new Rect (0, 24, position.width / zoom, position.height / zoom - 24), scrollPosition, new Rect (0, 0, maxScroll.x, maxScroll.y), false, false);
		
		BeginWindows ();
		
		canMarquee = true;
		Vector2 newMaxScroll = Vector2.zero;
		for (int i=0; i<numActions; i++)
		{
			FixConnections (i, isAsset);
			
			Action _action;
			if (isAsset)
			{
				_action = _targetAsset.actions[i];
			}
			else
			{
				_action = _target.actions[i];
			}

			if (i == 0)
			{
				GUI.Label (new Rect (16, -2, 100, 20), "START", nodeSkin.label);

				if (_action.nodeRect.x == 50 && _action.nodeRect.y == 50)
				{
					// Upgrade
					_action.nodeRect.x = _action.nodeRect.y = 14;
					MarkAll (isAsset);
					PerformEmptyCallBack ("Expand selected");
					UnmarkAll (isAsset);
				}
			}
			
			Color tempColor = GUI.color;
			if (_action.isRunning && Application.isPlaying)
			{
				GUI.color = Color.cyan;
			}
			else if (actionChanging != null && _action.nodeRect.Contains (e.mousePosition))
			{
				GUI.color = new Color (1f, 1f, 0.1f);
			}
			else if (_action.isMarked)
			{
				GUI.color = new Color (0.7f, 1f, 0.6f);
			}
			
			Vector2 originalPosition = new Vector2 (_action.nodeRect.x, _action.nodeRect.y);

			string label = i + ": " + _action.title;
			if (!_action.isDisplayed)
			{
				GUI.skin = emptyNodeSkin;
				_action.nodeRect.height = 21f;
				string extraLabel = _action.SetLabel ();
				if (extraLabel.Length > 15)
				{
					extraLabel = extraLabel.Substring (0, 15) + "..)";
				}
				label += extraLabel;
				_action.nodeRect = GUI.Window (i, _action.nodeRect, EmptyNodeWindow, label);
			}
			else 
			{
				GUI.skin = nodeSkin;

				if (GUI.changed) 
				{
					_action.nodeRect.height = 10;
				}

				_action.nodeRect = GUILayout.Window (i, _action.nodeRect, NodeWindow, label, GUILayout.Width (300));
			}

			Vector2 finalPosition = new Vector2 (_action.nodeRect.x, _action.nodeRect.y);
			if (finalPosition != originalPosition)
			{
				if (isAsset)
				{
					DragNodes (_action, _targetAsset.actions, finalPosition - originalPosition);
				}
				else
				{
					DragNodes (_action, _target.actions, finalPosition - originalPosition);
				}
			}	
				
			GUI.skin = null;
			GUI.color = tempColor;
			
			if (_action.nodeRect.x + _action.nodeRect.width + 20 > newMaxScroll.x)
			{
				newMaxScroll.x = _action.nodeRect.x + _action.nodeRect.width + 20;
			}
			if (_action.nodeRect.height != 10)
			{
				if (_action.nodeRect.y + _action.nodeRect.height + 100 > newMaxScroll.y)
				{
					newMaxScroll.y = _action.nodeRect.y + _action.nodeRect.height + 100;
				}
			}

			LimitWindow (_action);
			DrawSockets (_action, isAsset);
			
			if (isAsset)
			{
				_targetAsset.actions = ActionListAssetEditor.ResizeList (_targetAsset, numActions);
			}
			else
			{
				_target.actions = ActionListEditor.ResizeList (_target.actions, numActions);
			}
			
			if (actionChanging != null && loseConnection && _action.nodeRect.Contains (e.mousePosition))
			{
				Reconnect (actionChanging, _action, isAsset);
			}
			
			if (!isMarquee && _action.nodeRect.Contains (e.mousePosition))
			{
				canMarquee = false;
			}
		}
		
		if (loseConnection && actionChanging != null)
		{
			EndConnect (actionChanging, e.mousePosition, isAsset);
		}
		
		if (actionChanging != null)
		{
			bool onSide = false;
			if (actionChanging is ActionCheck || actionChanging is ActionCheckMultiple)
			{
				onSide = true;
			}
			AdvGame.DrawNodeCurve (actionChanging.nodeRect, e.mousePosition, Color.cyan, offsetChanging, onSide, false, actionChanging.isDisplayed);
		}
		
		if (e.type == EventType.ContextClick && actionChanging == null && !isMarquee)
		{
			menuPosition = e.mousePosition;
			CreateEmptyMenu (isAsset);
		}
		
		EndWindows ();
		GUI.EndScrollView ();
		EditorZoomArea.End();
		
		if (newMaxScroll.y != 0)
		{
			maxScroll = newMaxScroll;
		}
	}


	private void DragNodes (Action dragAction, List<Action> actionList, Vector2 offset)
	{
		foreach (Action _action in actionList)
		{
			if (dragAction != _action && _action.isMarked)
			{
				_action.nodeRect.x += offset.x;
				_action.nodeRect.y += offset.y;
			}
		}
	}


	private void SetMarked (bool isAsset, bool state)
	{
		if (isAsset)
		{
			if (_targetAsset && _targetAsset.actions.Count > 0)
			{
				foreach (Action action in _targetAsset.actions)
				{
					if (action)
					{
						action.isMarked = state;
					}
				}
			}
		}
		else
		{
			if (_target && _target.actions.Count > 0)
			{
				foreach (Action action in _target.actions)
				{
					if (action)
					{
						action.isMarked = state;
					}
				}
			}
		}
	}
	
	
	private void UnmarkAll (bool isAsset)
	{
		SetMarked (isAsset, false);
	}


	private void MarkAll (bool isAsset)
	{
		SetMarked (isAsset, true);
	}
		
	
	private Action InsertAction (int i, Vector2 position, bool isAsset)
	{
		List<Action> actionList = new List<Action>();
		if (isAsset)
		{
			actionList = _targetAsset.actions;
			Undo.RecordObject (_targetAsset, "Create action");
			ActionListAssetEditor.AddAction (actionsManager.GetDefaultAction (), i+1, _targetAsset);
		}
		else
		{
			actionList = _target.actions;
			ActionListEditor.ModifyAction (_target, _target.actions[i], "Insert after");
		}
		
		numActions ++;
		UnmarkAll (isAsset);
		
		actionList [i+1].nodeRect.x = position.x - 150;
		actionList [i+1].nodeRect.y = position.y;
		actionList [i+1].endAction = ResultAction.Stop;
		actionList [i+1].isDisplayed = true;
		
		return actionList [i+1];
	}
	
	
	private void FixConnections (int i, bool isAsset)
	{
		List<Action> actionList = new List<Action>();
		if (isAsset)
		{
			actionList = _targetAsset.actions;
		}
		else
		{
			actionList = _target.actions;
		}
		
		if (actionList[i].numSockets == 0)
		{
			actionList[i].endAction = ResultAction.Stop;
		}
		
		else if (actionList[i] is ActionCheck)
		{
			ActionCheck tempAction = (ActionCheck) actionList[i];
			if (tempAction.resultActionTrue == ResultAction.Skip && !actionList.Contains (tempAction.skipActionTrueActual))
			{
				if (tempAction.skipActionTrue >= actionList.Count)
				{
					tempAction.resultActionTrue = ResultAction.Stop;
				}
			}
			if (tempAction.resultActionFail == ResultAction.Skip && !actionList.Contains (tempAction.skipActionFailActual))
			{
				if (tempAction.skipActionFail >= actionList.Count)
				{
					tempAction.resultActionFail = ResultAction.Stop;
				}
			}
		}
		else if (actionList[i] is ActionCheckMultiple)
		{
			ActionCheckMultiple tempAction = (ActionCheckMultiple) actionList[i];
			foreach (ActionEnd ending in tempAction.endings)
			{
				if (ending.resultAction == ResultAction.Skip && !actionList.Contains (ending.skipActionActual))
				{
					if (ending.skipAction >= actionList.Count)
					{
						ending.resultAction = ResultAction.Stop;
					}
				}
			}
		}
		else
		{
			if (actionList[i].endAction == ResultAction.Skip && !actionList.Contains (actionList[i].skipActionActual))
			{
				if (actionList[i].skipAction >= actionList.Count)
				{
					actionList[i].endAction = ResultAction.Stop;
				}
			}
		}
	}
	
	
	private void EndConnect (Action action1, Vector2 mousePosition, bool isAsset)
	{
		List<Action> actionList = new List<Action>();
		if (isAsset)
		{
			actionList = _targetAsset.actions;
		}
		else
		{
			actionList = _target.actions;
		}
		
		isMarquee = false;
		
		if (action1 is ActionCheck)
		{
			ActionCheck tempAction = (ActionCheck) action1;
			
			if (resultType)
			{
				if (actionList.IndexOf (action1) == actionList.Count - 1 && tempAction.resultActionTrue != ResultAction.Skip)
				{
					InsertAction (actionList.IndexOf (action1), mousePosition, isAsset);
					tempAction.resultActionTrue = ResultAction.Continue;
				}
				else if (tempAction.resultActionTrue == ResultAction.Stop)
				{
					tempAction.resultActionTrue = ResultAction.Skip;
					tempAction.skipActionTrueActual = InsertAction (actionList.Count-1, mousePosition, isAsset);
				}
				else
				{
					tempAction.resultActionTrue = ResultAction.Stop;
				}
			}
			else
			{
				if (actionList.IndexOf (action1) == actionList.Count - 1 && tempAction.resultActionFail != ResultAction.Skip)
				{
					InsertAction (actionList.IndexOf (action1), mousePosition, isAsset);
					tempAction.resultActionFail = ResultAction.Continue;
				}
				else if (tempAction.resultActionFail == ResultAction.Stop)
				{
					tempAction.resultActionFail = ResultAction.Skip;
					tempAction.skipActionFailActual = InsertAction (actionList.Count-1, mousePosition, isAsset);
				}
				else
				{
					tempAction.resultActionFail = ResultAction.Stop;
				}
			}
		}
		else if (action1 is ActionCheckMultiple)
		{
			ActionCheckMultiple tempAction = (ActionCheckMultiple) action1;
			ActionEnd ending = tempAction.endings [multipleResultType];
			
			if (actionList.IndexOf (action1) == actionList.Count - 1 && ending.resultAction != ResultAction.Skip)
			{
				InsertAction (actionList.IndexOf (action1), mousePosition, isAsset);
				ending.resultAction = ResultAction.Continue;
			}
			else if (ending.resultAction == ResultAction.Stop)
			{
				ending.resultAction = ResultAction.Skip;
				ending.skipActionActual = InsertAction (actionList.Count-1, mousePosition, isAsset);
			}
			else
			{
				ending.resultAction = ResultAction.Stop;
			}
		}
		else
		{
			if (actionList.IndexOf (action1) == actionList.Count - 1 && action1.endAction != ResultAction.Skip)
			{
				InsertAction (actionList.IndexOf (action1), mousePosition, isAsset);
				action1.endAction = ResultAction.Continue;
			}
			else if (action1.endAction == ResultAction.Stop)
			{
				// Remove bad "end" connection
				float x = mousePosition.x;
				foreach (AC.Action action in actionList)
				{
					if (action.nodeRect.x > x && !(action is ActionCheck) && !(action is ActionCheckMultiple) && action.endAction == ResultAction.Continue)
					{
						// Is this the "last" one?
						int i = actionList.IndexOf (action);
						if (actionList.Count == (i+1))
						{
							action.endAction = ResultAction.Stop;
						}
					}
				}
				
				action1.endAction = ResultAction.Skip;
				action1.skipActionActual = InsertAction (actionList.Count-1, mousePosition, isAsset);
			}
			else
			{
				action1.endAction = ResultAction.Stop;
			}
		}
		
		actionChanging = null;
		offsetChanging = 0;
		
		if (isAsset)
		{
			EditorUtility.SetDirty (_targetAsset);
		}
		else
		{
			EditorUtility.SetDirty (_target);
		}
	}
	
	
	private void Reconnect (Action action1, Action action2, bool isAsset)
	{
		isMarquee = false;
		
		if (action1 is ActionCheck)
		{
			ActionCheck actionCheck = (ActionCheck) action1;
			
			if (resultType)
			{
				actionCheck.resultActionTrue = ResultAction.Skip;
				if (action2 != null)
				{
					actionCheck.skipActionTrueActual = action2;
				}
			}
			else
			{
				actionCheck.resultActionFail = ResultAction.Skip;
				if (action2 != null)
				{
					actionCheck.skipActionFailActual = action2;
				}
			}
		}
		else if (action1 is ActionCheckMultiple)
		{
			ActionCheckMultiple actionCheckMultiple = (ActionCheckMultiple) action1;
			
			ActionEnd ending = actionCheckMultiple.endings [multipleResultType];
			
			ending.resultAction = ResultAction.Skip;
			if (action2 != null)
			{
				ending.skipActionActual = action2;
			}
		}
		else
		{
			action1.endAction = ResultAction.Skip;
			action1.skipActionActual = action2;
		}
		
		actionChanging = null;
		offsetChanging = 0;
		
		if (isAsset)
		{
			EditorUtility.SetDirty (_targetAsset);
		}
		else
		{
			EditorUtility.SetDirty (_target);
		}
	}
	
	
	private Rect SocketIn (Action action)
	{
		return new Rect (action.nodeRect.x - 20, action.nodeRect.y, 20, 20);
	}
	
	
	private void DrawSockets (Action action, bool isAsset)
	{
		List<Action> actionList = new List<Action>();
		if (isAsset)
		{
			actionList = _targetAsset.actions;
		}
		else
		{
			actionList = _target.actions;
		}
		
		int i = actionList.IndexOf (action);
		Event e = Event.current;
		
		if (action.numSockets == 0)
		{
			return;
		}
		
		if (!action.isDisplayed && (action is ActionCheck || action is ActionCheckMultiple))
		{
			action.DrawOutWires (actionList, i, 0);
			return;
		}
		
		int offset = 0;
		
		if (action is ActionCheck)
		{
			ActionCheck actionCheck = (ActionCheck) action;
			if (actionCheck.resultActionFail != ResultAction.RunCutscene)
			{
				Rect buttonRect = new Rect (action.nodeRect.x + action.nodeRect.width - 2, action.nodeRect.y - 22 + action.nodeRect.height, 16, 16);
				
				if (e.isMouse && actionChanging == null && e.type == EventType.MouseDown && action.isEnabled && buttonRect.Contains(e.mousePosition))
				{
					offsetChanging = 10;
					resultType = false;
					actionChanging = action;
				}
				
				GUI.Button (buttonRect, "", nodeSkin.customStyles[10]);
				
				if (actionCheck.resultActionFail == ResultAction.Skip)
				{
					offset = 17;
				}
			}
			if (actionCheck.resultActionTrue != ResultAction.RunCutscene)
			{
				Rect buttonRect = new Rect (action.nodeRect.x + action.nodeRect.width - 2, action.nodeRect.y - 40 - offset + action.nodeRect.height, 16, 16);
				
				if (e.isMouse && actionChanging == null && e.type == EventType.MouseDown && action.isEnabled && buttonRect.Contains(e.mousePosition))
				{
					offsetChanging = 30 + offset;
					resultType = true;
					actionChanging = action;
				}
				
				GUI.Button (buttonRect, "", nodeSkin.customStyles[10]);
			}
		}
		else if (action is ActionCheckMultiple)
		{
			ActionCheckMultiple actionCheckMultiple = (ActionCheckMultiple) action;
			
			foreach (ActionEnd ending in actionCheckMultiple.endings)
			{
				int j = actionCheckMultiple.endings.IndexOf (ending);
				
				if (ending.resultAction != ResultAction.RunCutscene)
				{
					Rect buttonRect = new Rect (action.nodeRect.x + action.nodeRect.width - 2, action.nodeRect.y + (j * 43) - (actionCheckMultiple.endings.Count * 43) + action.nodeRect.height, 16, 16);
					
					if (e.isMouse && actionChanging == null && e.type == EventType.MouseDown && action.isEnabled && buttonRect.Contains(e.mousePosition))
					{
						offsetChanging = (actionCheckMultiple.endings.Count - j) * 43 - 13;
						multipleResultType = actionCheckMultiple.endings.IndexOf (ending);
						actionChanging = action;
					}
					
					GUI.Button (buttonRect, "", nodeSkin.customStyles[10]);
				}
			}
		}
		else
		{
			if (action.endAction != ResultAction.RunCutscene)
			{
				Rect buttonRect = new Rect (action.nodeRect.x + action.nodeRect.width / 2f - 8, action.nodeRect.y + action.nodeRect.height, 16, 16);
				
				if (e.isMouse && actionChanging == null && e.type == EventType.MouseDown && action.isEnabled && buttonRect.Contains(e.mousePosition))
				{
					offsetChanging = 10;
					actionChanging = action;
				}
				
				GUI.Button (buttonRect, "", nodeSkin.customStyles[10]);
			}
		}
		
		action.DrawOutWires (actionList, i, offset);
	}
	
	
	private int GetTypeNumber (int i, bool isAsset)
	{
		List<Action> actionList = new List<Action>();
		if (isAsset)
		{
			actionList = _targetAsset.actions;
		}
		else
		{
			actionList = _target.actions;
		}
		
		int number = 0;
		ActionsManager actionsManager = AdvGame.GetReferences ().actionsManager;
		if (actionsManager)
		{
			for (int j=0; j<actionsManager.GetActionsSize(); j++)
			{
				try
				{
					if (actionList[i].GetType ().ToString () == actionsManager.GetActionName (j) || actionList[i].GetType ().ToString () == ("AC." + actionsManager.GetActionName (j)))
					{
						number = j;
						break;
					}
				}
				
				catch
				{
					string defaultAction = actionsManager.GetDefaultAction ();
					Action newAction = (Action) CreateInstance (defaultAction);
					actionList[i] = newAction;
					
					if (isAsset)
					{
						AssetDatabase.AddObjectToAsset (newAction, _targetAsset);
					}
				}
			}
		}
		
		return number;
	}
	
	
	private int GetCategoryNumber (int i)
	{
		if (actionsManager)
		{
			return actionsManager.GetActionCategory (i);
		}
		
		return 0;
	}
	
	
	private int GetSubCategoryNumber (int i, int _categoryNumber, bool isAsset)
	{
		int number = 0;
		
		if (actionsManager)
		{
			if (isAsset)
			{
				return actionsManager.GetActionSubCategory (_targetAsset.actions[i].title, _categoryNumber);
			}
			else
			{
				return actionsManager.GetActionSubCategory (_target.actions[i].title, _categoryNumber);
			}
		}
		
		return number;
	}
	
	
	private int NumActionsMarked (bool isAsset)
	{
		List<Action> actionList = new List<Action>();
		if (isAsset)
		{
			actionList = _targetAsset.actions;
		}
		else
		{
			actionList = _target.actions;
		}
		
		int i=0;
		foreach (Action action in actionList)
		{
			if (action.isMarked)
			{
				i++;
			}
		}
		
		return i;
	}
	
	
	private void CreateEmptyMenu (bool isAsset)
	{
		GenericMenu menu = new GenericMenu ();
		menu.AddItem (new GUIContent ("Add new Action"), false, EmptyCallback, "Add new Action");
		if (AdvGame.copiedActions != null && AdvGame.copiedActions.Count > 0)
		{
			menu.AddSeparator ("");
			menu.AddItem (new GUIContent ("Paste copied Action(s)"), false, EmptyCallback, "Paste copied Action(s)");
		}
		
		menu.AddSeparator ("");
		menu.AddItem (new GUIContent ("Select all"), false, EmptyCallback, "Select all");
		
		if (NumActionsMarked (isAsset) > 0)
		{
			menu.AddItem (new GUIContent ("Deselect all"), false, EmptyCallback, "Deselect all");
			menu.AddSeparator ("");
			menu.AddItem (new GUIContent ("Copy selected"), false, EmptyCallback, "Copy selected");
			menu.AddItem (new GUIContent ("Delete selected"), false, EmptyCallback, "Delete selected");
			menu.AddSeparator ("");
			menu.AddItem (new GUIContent ("Collapse selected"), false, EmptyCallback, "Collapse selected");
			menu.AddItem (new GUIContent ("Expand selected"), false, EmptyCallback, "Expand selected");

			if (NumActionsMarked (isAsset) == 1)
			{
				menu.AddSeparator ("");
				menu.AddItem (new GUIContent ("Move to front"), false, EmptyCallback, "Move to front");
			}
		}
		
		menu.AddSeparator ("");
		menu.AddItem (new GUIContent ("Auto-arrange"), false, EmptyCallback, "Auto-arrange");
		
		menu.ShowAsContext ();
	}
	
	
	private void CreateNodeMenu (bool isAsset, int i, Action _action)
	{
		UnmarkAll (isAsset);
		_action.isMarked = true;
		
		GenericMenu menu = new GenericMenu ();
		
		menu.AddItem (new GUIContent ("Copy"), false, EmptyCallback, "Copy selected");
		menu.AddItem (new GUIContent ("Delete"), false, EmptyCallback, "Delete selected");
		
		if (i>0)
		{
			menu.AddSeparator ("");
			menu.AddItem (new GUIContent ("Move to front"), false, EmptyCallback, "Move to front");
		}
		
		menu.ShowAsContext ();
	}
	

	private void EmptyCallback (object obj)
	{
		PerformEmptyCallBack (obj.ToString ());
	}

	private void PerformEmptyCallBack (string objString)
	{
		bool isAsset = false;
		List<Action> actionList = new List<Action>();
		if (_targetAsset != null)
		{
			isAsset = true;
			actionList = _targetAsset.actions;
			Undo.RecordObject (_targetAsset, objString);
		}
		else
		{
			actionList = _target.actions;
			Undo.RecordObject (_target, objString);
		}
		
		if (objString == "Add new Action")
		{
			Action currentAction = actionList [actionList.Count-1];
			if (currentAction.endAction == ResultAction.Continue)
			{
				currentAction.endAction = ResultAction.Stop;
			}
			
			if (isAsset)
			{
				ActionListAssetEditor.ModifyAction (_targetAsset, currentAction, "Insert after");
			}
			else
			{
				ActionListEditor.ModifyAction (_target, null, "Insert end");
			}
			
			actionList[actionList.Count-1].nodeRect.x = menuPosition.x;
			actionList[actionList.Count-1].nodeRect.y = menuPosition.y;
			actionList[actionList.Count-1].isDisplayed = true;
		}
		else if (objString == "Paste copied Action(s)")
		{
			if (AdvGame.copiedActions.Count == 0)
			{
				return;
			}

			UnmarkAll (isAsset);

			Action currentLastAction = actionList [actionList.Count-1];
			if (currentLastAction.endAction == ResultAction.Continue)
			{
				currentLastAction.endAction = ResultAction.Stop;
			}
			
			List<Action> pasteList = AdvGame.copiedActions;
			Vector2 firstPosition = new Vector2 (pasteList[0].nodeRect.x, pasteList[0].nodeRect.y);
			foreach (Action pasteAction in pasteList)
			{
				if (pasteList.IndexOf (pasteAction) == 0)
				{
					pasteAction.nodeRect.x = menuPosition.x;
					pasteAction.nodeRect.y = menuPosition.y;
				}
				else
				{
					pasteAction.nodeRect.x = menuPosition.x + (pasteAction.nodeRect.x - firstPosition.x);
					pasteAction.nodeRect.y = menuPosition.y + (pasteAction.nodeRect.y - firstPosition.y);
				}
				if (isAsset)
				{
					pasteAction.hideFlags = HideFlags.HideInHierarchy;
					AssetDatabase.AddObjectToAsset (pasteAction, _targetAsset);
				}
				pasteAction.isMarked = true;
				actionList.Add (pasteAction);
			}
			if (isAsset)
			{
				AssetDatabase.SaveAssets ();
			}
			AdvGame.DuplicateActionsBuffer ();
		}
		else if (objString == "Select all")
		{
			foreach (Action action in actionList)
			{
				action.isMarked = true;
			}
		}
		else if (objString == "Deselect all")
		{
			foreach (Action action in actionList)
			{
				action.isMarked = false;
			}
		}
		else if (objString == "Expand selected")
		{
			foreach (Action action in actionList)
			{
				if (action.isMarked)
				{
					action.isDisplayed = true;
				}
			}
		}
		else if (objString == "Collapse selected")
		{
			foreach (Action action in actionList)
			{
				if (action.isMarked)
				{
					action.isDisplayed = false;
				}
			}
		}
		else if (objString == "Cut selected" || objString == "Copy selected")
		{
			List<Action> copyList = new List<Action>();
			foreach (Action action in actionList)
			{
				if (action.isMarked)
				{
					Action copyAction = Object.Instantiate (action) as Action;
					copyAction.PrepareToCopy (actionList.IndexOf (action), actionList);
					copyAction.isMarked = false;
					copyList.Add (copyAction);
				}
			}
			
			foreach (Action copyAction in copyList)
			{
				copyAction.AfterCopy (copyList);
			}
			
			AdvGame.copiedActions = copyList;

			if (objString == "Cut selected")
			{
				PerformEmptyCallBack ("Delete selected");
			}
			else
			{
				UnmarkAll (isAsset);
			}
		}
		else if (objString == "Delete selected")
		{
			while (NumActionsMarked (isAsset) > 0)
			{
				foreach (Action action in actionList)
				{
					if (action.isMarked)
					{
						// Work out what has to be re-connected to what after deletion
						Action targetAction = null;
						if (action is ActionCheck || action is ActionCheckMultiple) {}
						else
						{
							if (action.endAction == ResultAction.Skip && action.skipActionActual)
							{
								targetAction = action.skipActionActual;
							}
							else if (action.endAction == ResultAction.Continue && actionList.IndexOf (action) < (actionList.Count - 1))
							{
								targetAction = actionList [actionList.IndexOf (action)+1];
							}
							
							foreach (Action _action in actionList)
							{
								if (action != _action)
								{
									_action.FixLinkAfterDeleting (action, targetAction, actionList);
								}
							}
						}
						
						if (isAsset)
						{
							ActionListAssetEditor.DeleteAction (action, _targetAsset);
						}
						else
						{
							actionList.Remove (action);
						}
						
						numActions --;
						//DestroyImmediate (action);
						if (action != null)
						{
							Undo.DestroyObjectImmediate (action);
						}
						break;
					}
				}
			}
			if (actionList.Count == 0)
			{
				if (isAsset)
				{
					actionList.Add (ActionListEditor.GetDefaultAction ());
				}
			}
		}
		else if (objString == "Move to front")
		{
			for (int i=0; i<actionList.Count; i++)
			{
				Action action = actionList[i];
				if (action.isMarked)
				{
					action.isMarked = false;
					if (i > 0)
					{
						if (action is ActionCheck || action is ActionCheckMultiple)
						{}
						else if (action.endAction == ResultAction.Continue && (i == actionList.Count - 1))
						{
							action.endAction = ResultAction.Stop;
						}
						
						actionList[0].nodeRect.x += 30f;
						actionList[0].nodeRect.y += 30f;
						actionList.Remove (action);
						actionList.Insert (0, action);
					}
				}
			}
		}
		else if (objString == "Auto-arrange")
		{
			AutoArrange (isAsset);
		}
		
		if (isAsset)
		{
			EditorUtility.SetDirty (_targetAsset);
		}
		else
		{
			EditorUtility.SetDirty (_target);
		}
	}
	
	
	private void AutoArrange (bool isAsset)
	{
		isAutoArranging = true;

		List<Action> actionList = new List<Action>();
		if (isAsset)
		{
			actionList = _targetAsset.actions;
		}
		else
		{
			actionList = _target.actions;
		}
		
		foreach (Action action in actionList)
		{
			action.isMarked = true;
			if (actionList.IndexOf (action) != 0)
			{
				action.nodeRect.x = action.nodeRect.y = -10;
			}
		}
		
		DisplayActionsInEditor _display = DisplayActionsInEditor.ArrangedVertically;
		if (AdvGame.GetReferences ().actionsManager && AdvGame.GetReferences ().actionsManager.displayActionsInEditor == DisplayActionsInEditor.ArrangedHorizontally)
		{
			_display = DisplayActionsInEditor.ArrangedHorizontally;
		}

		ArrangeFromIndex (actionList, 0, 0, 14, _display);

		int i=1;
		float maxValue = 0f;
		foreach (Action _action in actionList)
		{
			if (_display == DisplayActionsInEditor.ArrangedVertically)
			{
				maxValue = Mathf.Max (maxValue, _action.nodeRect.y + _action.nodeRect.height);
			}
			else
			{
				maxValue = Mathf.Max (maxValue, _action.nodeRect.x);
			}
		}

		foreach (Action _action in actionList)
		{
			if (_action.isMarked)
			{
				// Wasn't arranged
				if (_display == DisplayActionsInEditor.ArrangedVertically)
				{
					_action.nodeRect.x = 14;
					_action.nodeRect.y = maxValue + 14*i;
					ArrangeFromIndex (actionList, actionList.IndexOf (_action), 0, 14, _display);
				}
				else
				{
					_action.nodeRect.x = maxValue + 350*i;
					_action.nodeRect.y = 14;
					ArrangeFromIndex (actionList, actionList.IndexOf (_action), 0, 14, _display);
				}
				_action.isMarked = false;
				i++;
			}
		}

		isAutoArranging = false;
	}
	
	
	private void ArrangeFromIndex (List<Action> actionList, int i, int depth, float minValue, DisplayActionsInEditor _display)
	{
		while (i > -1 && actionList.Count > i)
		{
			Action _action = actionList[i];
			
			if (i > 0 && _action.isMarked)
			{
				if (_display == DisplayActionsInEditor.ArrangedVertically)
				{
					_action.nodeRect.x = 14 + (350 * depth);

					// Find top-most Y position
					float yPos = minValue;
					bool doAgain = true;
					
					while (doAgain)
					{
						int numChanged = 0;
						foreach (Action otherAction in actionList)
						{
							if (otherAction != _action && otherAction.nodeRect.x == _action.nodeRect.x && otherAction.nodeRect.y >= yPos)
							{
								yPos = otherAction.nodeRect.y + otherAction.nodeRect.height + 30f;
								numChanged ++;
							}
						}
						
						if (numChanged == 0)
						{
							doAgain = false;
						}
					}
					_action.nodeRect.y = yPos;
				}
				else
				{
					_action.nodeRect.y = 14 + (260 * depth);

					// Find left-most X position
					float xPos = minValue + 350;
					bool doAgain = true;
					
					while (doAgain)
					{
						int numChanged = 0;
						foreach (Action otherAction in actionList)
						{
							if (otherAction != _action && otherAction.nodeRect.x == xPos && otherAction.nodeRect.y == _action.nodeRect.y)
							{
								xPos += 350;
								numChanged ++;
							}
						}
						
						if (numChanged == 0)
						{
							doAgain = false;
						}
					}
					_action.nodeRect.x = xPos;
				}
			}
			
			if (_action.isMarked == false)
			{
				return;
			}
			
			_action.isMarked = false;

			float newMinValue = 0f;
			if (_display == DisplayActionsInEditor.ArrangedVertically)
			{
				newMinValue = _action.nodeRect.y + _action.nodeRect.height + 30f;
			}
			else
			{
				newMinValue = _action.nodeRect.x;
			}
			
			if (_action is ActionCheckMultiple)
			{
				ActionCheckMultiple _actionCheckMultiple = (ActionCheckMultiple) _action;
				
				for (int j=_actionCheckMultiple.endings.Count-1; j>=0; j--)
				{
					ActionEnd ending = _actionCheckMultiple.endings [j];
					if (j > 0)
					{
						if (ending.resultAction == ResultAction.Skip)
						{
							ArrangeFromIndex (actionList, actionList.IndexOf (ending.skipActionActual), depth+j, newMinValue, _display);
						}
						else if (ending.resultAction == ResultAction.Continue)
						{
							ArrangeFromIndex (actionList, i+1, depth+j, newMinValue, _display);
						}
					}
					else
					{
						if (ending.resultAction == ResultAction.Skip)
						{
							i = actionList.IndexOf (ending.skipActionActual);
						}
						else if (ending.resultAction == ResultAction.Continue)
						{
							i++;
						}
						else
						{
							i = -1;
						}
					}
				}
			}
			else if (_action is ActionCheck)
			{
				ActionCheck _actionCheck = (ActionCheck) _action;

				if (_actionCheck.resultActionFail == ResultAction.Stop || _actionCheck.resultActionFail == ResultAction.RunCutscene)
				{
					if (_actionCheck.resultActionTrue == ResultAction.Skip)
					{
						i = actionList.IndexOf (_actionCheck.skipActionTrueActual);
					}
					else if (_actionCheck.resultActionTrue == ResultAction.Continue)
					{
						i++;
					}
					else
					{
						i = -1;
					}
				}
				else
				{
					if (_actionCheck.resultActionTrue == ResultAction.Skip)
					{
						ArrangeFromIndex (actionList, actionList.IndexOf (_actionCheck.skipActionTrueActual), depth+1, newMinValue, _display);
					}
					else if (_actionCheck.resultActionTrue == ResultAction.Continue)
					{
						ArrangeFromIndex (actionList, i+1, depth+1, newMinValue, _display);
					}
					
					if (_actionCheck.resultActionFail == ResultAction.Skip)
					{
						i = actionList.IndexOf (_actionCheck.skipActionFailActual);
					}
					else if (_actionCheck.resultActionFail == ResultAction.Continue)
					{
						i++;
					}
					else
					{
						i = -1;
					}
				}
			}
			else
			{
				if (_action.endAction == ResultAction.Skip)
				{
					i = actionList.IndexOf (_action.skipActionActual);
				}
				else if (_action.endAction == ResultAction.Continue)
				{
					i++;
				}
				else
				{
					i = -1;
				}
			}
		}
	}

}


