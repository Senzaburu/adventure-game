﻿/*
 *
 *	Adventure Creator
 *	by Chris Burton, 2013-2014
 *	
 *	"ActionListManager.cs"
 * 
 *	This script keeps track of which ActionLists
 *	are running in a scene.
 * 
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AC
{
	
	public class ActionListManager : MonoBehaviour
	{
		
		private bool playCutsceneOnVarChange = false;
		private bool saveAfterCutscene = false;
		
		private Conversation conversationOnEnd;
		private List<ActionList> activeLists = new List<ActionList>();

		
		private void Awake ()
		{
			activeLists.Clear ();
		}
		
		
		public void UpdateActionListManager ()
		{
			if (saveAfterCutscene && !IsGameplayBlocked ())
			{
				saveAfterCutscene = false;
				SaveSystem.SaveAutoSave ();
			}
			
			if (playCutsceneOnVarChange && KickStarter.stateHandler && (KickStarter.stateHandler.gameState == GameState.Normal || KickStarter.stateHandler.gameState == GameState.DialogOptions))
			{
				playCutsceneOnVarChange = false;
				
				if (KickStarter.sceneSettings.cutsceneOnVarChange != null)
				{
					KickStarter.sceneSettings.cutsceneOnVarChange.Interact ();
				}
			}
		}
		
		
		public void EndCutscene ()
		{
			if (!IsGameplayBlocked ())
			{
				return;
			}
			
			if (AdvGame.GetReferences ().settingsManager.blackOutWhenSkipping)
			{
				KickStarter.mainCamera.HideScene ();
			}
			
			// Stop all non-looping sound
			Sound[] sounds = FindObjectsOfType (typeof (Sound)) as Sound[];
			foreach (Sound sound in sounds)
			{
				if (sound.GetComponent <AudioSource>())
				{
					if (sound.soundType != SoundType.Music && !sound.GetComponent <AudioSource>().loop)
					{
						sound.Stop ();
					}
				}
			}

			List<ActionList> originalSkipLists = new List<ActionList>();
			foreach (ActionList activeList in activeLists)
			{
				if (activeList.isSkippable && activeList.actionListType == ActionListType.PauseGameplay)
				{
					originalSkipLists.Add (activeList);
				}
			}

			while (originalSkipLists.Count > 0)
			{
				ActionList[] killedLists = originalSkipLists[0].Skip ();
				originalSkipLists.RemoveAt (0);
				
				foreach (ActionList killedList in killedLists)
				{
					if (killedList != null && originalSkipLists.Contains (killedList))
					{
						originalSkipLists.Remove (killedList);
					}
				}
			}

		}
		
		
		public bool AreActionListsRunning ()
		{
			if (activeLists.Count > 0)
			{
				return true;
			}
			return false;
		}
		
		
		#if UNITY_EDITOR
		
		private void OnGUI ()
		{
			if (KickStarter.settingsManager.showActiveActionLists)
			{
				if (activeLists.Count > 0)
				{
					GUILayout.BeginVertical ("Button");
					GUILayout.Label ("Current ActionLists running:");
					GUILayout.Space (4f);
					
					foreach (ActionList list in activeLists)
					{
						GUILayout.Label (list.gameObject.name, "Button");
					}

					if (IsGameplayBlocked ())
					{
						GUILayout.Space (4f);
						GUILayout.Label ("Gameplay is blocked");
					}
					GUILayout.EndVertical ();
				}
			}
		}
		
		#endif
		
		
		public void AddToList (ActionList _list)
		{
			if (!IsListRunning (_list))
			{
				activeLists.Add (_list);
			}
			
			if (_list.conversation)
			{
				conversationOnEnd = _list.conversation;
			}
			
			if (_list is RuntimeActionList && _list.actionListType == ActionListType.PauseGameplay && !_list.unfreezePauseMenus && KickStarter.playerMenus.ArePauseMenusOn (null))
			{
				// Don't affect the gamestate if we want to remain frozen
				return;
			}
			
			SetCorrectGameState ();
		}
		
		
		public void EndList (ActionList _list)
		{
			if (IsListRunning (_list))
			{
				activeLists.Remove (_list);
			}
			
			_list.Reset ();
			
			if (_list.conversation == conversationOnEnd && _list.conversation != null)
			{
				if (KickStarter.stateHandler)
				{
					KickStarter.stateHandler.gameState = GameState.Cutscene;
				}
				else
				{
					Debug.LogWarning ("Could not set correct GameState!");
				}
				
				conversationOnEnd.Interact ();
				conversationOnEnd = null;
			}
			else
			{
				if (_list is RuntimeActionList && _list.actionListType == ActionListType.PauseGameplay && !_list.unfreezePauseMenus && KickStarter.playerMenus.ArePauseMenusOn (null))
				{
					// Don't affect the gamestate if we want to remain frozen
				}
				else
				{
					SetCorrectGameStateEnd ();
				}
			}
			
			if (_list.autosaveAfter)
			{
				if (!IsGameplayBlocked ())
				{
					SaveSystem.SaveAutoSave ();
				}
				else
				{
					saveAfterCutscene = true;
				}
			}
			
			if (_list is RuntimeActionList)
			{
				RuntimeActionList runtimeActionList = (RuntimeActionList) _list;
				runtimeActionList.DestroySelf ();
			}
		}
		
		
		public void EndAssetList (string assetName)
		{
			RuntimeActionList[] runtimeActionLists = FindObjectsOfType (typeof (RuntimeActionList)) as RuntimeActionList[];
			foreach (RuntimeActionList runtimeActionList in runtimeActionLists)
			{
				if (runtimeActionList.name == assetName)
				{
					EndList (runtimeActionList);
				}
			}
		}
		
		
		public void VariableChanged ()
		{
			playCutsceneOnVarChange = true;
		}
		
		
		public bool IsListRunning (ActionList _list)
		{
			foreach (ActionList list in activeLists)
			{
				if (list == _list)
				{
					return true;
				}
			}
			
			return false;
		}
		
		
		public void KillAllLists ()
		{
			foreach (ActionList _list in activeLists)
			{
				_list.Reset ();
				
				if (_list is RuntimeActionList)
				{
					RuntimeActionList runtimeActionList = (RuntimeActionList) _list;
					runtimeActionList.DestroySelf ();
				}
			}
			
			activeLists.Clear ();
		}
		
		
		public static void KillAll ()
		{
			KickStarter.actionListManager.KillAllLists ();
		}
		
		
		private void SetCorrectGameStateEnd ()
		{
			if (KickStarter.stateHandler != null)
			{
				if (KickStarter.playerMenus.ArePauseMenusOn (null))
				{
					KickStarter.mainCamera.PauseGame ();
				}
				else
				{
					KickStarter.stateHandler.RestoreLastNonPausedState ();
				}
			}
			else
			{
				Debug.LogWarning ("Could not set correct GameState!");
			}
		}

		
		private void SetCorrectGameState ()
		{
			if (KickStarter.stateHandler != null)
			{
				if (IsGameplayBlocked ())
				{
					if (KickStarter.stateHandler.gameState != GameState.Cutscene)
					{
						KickStarter.runtimeVariables.BackupAllValues ();
						KickStarter.localVariables.BackupAllValues ();
					}
					KickStarter.stateHandler.gameState = GameState.Cutscene;
				}
				else if (KickStarter.playerMenus.ArePauseMenusOn (null))
				{
					KickStarter.stateHandler.gameState = GameState.Paused;
				}
				else
				{
					if (KickStarter.playerInput.activeConversation != null)
					{
						KickStarter.stateHandler.gameState = GameState.DialogOptions;
					}
					else
					{
						KickStarter.stateHandler.gameState = GameState.Normal;
					}
				}
			}
			else
			{
				Debug.LogWarning ("Could not set correct GameState!");
			}
		}
		
		
		public bool IsGameplayBlocked ()
		{
			foreach (ActionList list in activeLists)
			{
				if (list.actionListType == ActionListType.PauseGameplay)
				{
					return true;
				}
			}
			return false;
		}
		
		
		public bool IsInSkippableCutscene ()
		{
			foreach (ActionList list in activeLists)
			{
				if (list.actionListType == AC.ActionListType.PauseGameplay && list.isSkippable)
				{
					return true;
				}
			}
			
			return false;
		}
		
		
		private void OnDestroy ()
		{
			activeLists.Clear ();
		}
		
	}
	
}